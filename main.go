package main

import (
	"./control"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	//data, err := ioutil.ReadFile("runtime.txt")
	//check(err)
	//body := parse.GetBody(string(data))
	//fmt.Println(parse.GetVar(body))
	r := gin.Default()
	r.Use(cors.Default())
	r.GET("/graph/hop", control.GraphHop)
	r.GET("/graph/runtime", control.GraphRuntime)
	r.GET("/scalar", control.Scalar)
	_ = r.Run(":12344")
}
