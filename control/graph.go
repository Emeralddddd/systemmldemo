package control

import (
	"../parse/hops"
	"../parse/runtime"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
)

func GraphHop(c *gin.Context) {
	data, err := ioutil.ReadFile("./hops.txt")
	if err != nil {
		panic(err)
	}
	block := hops.Split(string(data))[3]
	vars := hops.GetTread(block)
	ops := hops.GetOp(block)
	outvars := hops.GetTwrite(block)
	c.JSON(http.StatusOK, gin.H{"vars": vars, "ops": ops, "outvars": outvars})
}

func GraphRuntime(c *gin.Context) {
	data, err := ioutil.ReadFile("./runtime.txt")
	if err != nil {
		panic(err)
	}
	block := runtime.Split(string(data))[3]
	vars := runtime.GetVars(string(data))
	ops := runtime.GetOps(block)
	outvars := runtime.GetAssign(ops)
	ops = runtime.Trim(ops)
	c.JSON(http.StatusOK, gin.H{"vars": vars, "ops": ops, "outvars": outvars})
}
