package control

import (
	"../parse"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Scalar(c *gin.Context) {
	data := parse.GenTestData()
	c.JSON(http.StatusOK, gin.H{"data": data})
}
