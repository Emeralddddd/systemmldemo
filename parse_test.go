package main

import (
	"./parse"
	"./parse/hops"
	"./parse/runtime"
	"io/ioutil"
	"testing"
)

func TestSplit(t *testing.T) {
	hops_text, _ := ioutil.ReadFile("hops.txt")
	rt := hops.Split(string(hops_text))
	print(rt)
}

func TestSplit1(t *testing.T) {
	runtime_text, _ := ioutil.ReadFile("runtime.txt")
	rt := runtime.Split(string(runtime_text))
	print(rt)
}

func TestGetVar1(t *testing.T) {
	runtime_text, _ := ioutil.ReadFile("runtime.txt")
	varMap := runtime.GetVars(string(runtime_text))
	opList := runtime.GetOps(runtime.Split(string(runtime_text))[3])
	runtime.GetAssign(opList)
	opList = runtime.Trim(opList)
	println(varMap)
}

func TestGetVar(t *testing.T) {
	hops_text, _ := ioutil.ReadFile("hops.txt")
	rt := hops.Split(string(hops_text))
	varMap := hops.GetTread(rt[3])
	println(varMap[3])
	Ops := hops.GetOp(rt[3])
	println(Ops)
	Outops := hops.GetTwrite(rt[3])
	println(Outops)
}

func TestOne(t *testing.T) {
	data := parse.GenTestData()
	println(data)
}
