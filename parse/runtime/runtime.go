package runtime

import (
	"regexp"
	"strconv"
	"strings"
)

var blockList []string
var blockSet = make(map[string]struct{})

func init() {
	blockList = append(blockList, "rmvar", "createvar", "print")
	for _, x := range blockList {
		blockSet[x] = struct{}{}
	}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func Split(data string) []string {
	r, err := regexp.Compile("GENERIC.+")
	check(err)
	result := r.Split(data, -1)
	return result
}

func GetVars(data string) []*Var {
	r, err := regexp.Compile("-+(\\S+) createvar (\\S+) (\\S+) (\\S+) (\\S+) (\\S+) ([\\d\\s]+)")
	check(err)
	varMap := make(map[string]*Var)
	for i, x := range r.FindAllStringSubmatch(data, -1) {
		newVar := Var{
			Index:  strconv.Itoa(i),
			Symbol: x[2],
			Shape:  x[7],
			Node:   x[1],
		}
		varMap[x[2]] = &newVar
	}
	r, err = regexp.Compile("cpvar (\\S+) (\\S+)")
	check(err)
	for _, x := range r.FindAllStringSubmatch(data, -1) {
		v := varMap[x[1]]
		v.Symbol = x[2]
		varMap[x[2]] = &Var{
			Index:  v.Index + "0",
			Symbol: v.Symbol,
			Shape:  v.Shape,
			Node:   v.Node,
		}
		//delete(varMap, x[1])
	}
	var varList []*Var
	for label, value := range varMap {
		varList = append(varList, &Var{
			Index:  value.Index,
			Symbol: value.Symbol,
			Label:  label,
			Shape:  value.Shape,
			Node:   value.Node,
		})
	}
	return varList
}

func GetOps(block string) []*Op {
	r1, err := regexp.Compile("-+(\\S+) (\\S+)(( \\S+\\.\\S+\\.\\S+)+)")
	//r2, err := regexp.Compile("(.*)\\.[SCALAR|MATRIX].\\S+")
	r2, err := regexp.Compile("(.*)\\.(SCALAR|MATRIX)")
	check(err)
	var opList []*Op
	for i, x := range r1.FindAllStringSubmatch(block, -1) {

		v := strings.Split(strings.TrimSpace(x[3]), " ")
		target := strings.Split(v[len(v)-1], ".")[0]
		source := ""
		for _, y := range v[:len(v)-1] {
			tmp := r2.FindStringSubmatch(y)
			source += tmp[1]
			source += " "
		}
		source = strings.TrimSpace(source)
		newop := Op{
			Index:  strconv.Itoa(i + 100),
			Symbol: x[2],
			Source: source,
			Target: target,
			Node:   x[1],
		}
		opList = append(opList, &newop)
	}
	return opList
}

func GetAssign(opList []*Op) []*OutVar {
	var outVarList []*OutVar
	for i, x := range opList {
		if x.Symbol == "assignvar" {
			newOutVar := OutVar{
				Index:  strconv.Itoa(i + 200),
				Symbol: x.Target,
				Source: x.Source,
				Node:   x.Node,
			}
			outVarList = append(outVarList, &newOutVar)
		}
	}
	return outVarList
}

func Trim(opList []*Op) []*Op {
	var newOpList []*Op
	for _, x := range opList {
		if !(x.Symbol == "assignvar" || x.Symbol == "print") {
			newOpList = append(newOpList, x)
		}
	}
	return newOpList
}

//func GetOps(block string) {
//	r, err := regexp.Compile("-+(\\S+) (\\S+) (.*)")
//	check(err)
//
//	var opList []Op
//	for i, x := range r.FindAllStringSubmatch(block, -1) {
//		node := x[1]
//		if node != "CP" && node != "MR" {
//			break
//		}
//		symbol := x[2]
//		if _,ok := blockSet[symbol];ok{
//			break
//		}
//		v := strings.Split(x[3], " ")
//		target := v[len(v)-1]
//		source :=  strings.Join(v[:len(v)-1]," ")
//		newOp := Op{
//			Index:  strconv.Itoa(i),
//			Symbol: symbol,
//			Source: source,
//			Target: target,
//			Node:   node,
//		}
//	}
//}
