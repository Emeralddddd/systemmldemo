package runtime

type Var struct {
	Index  string `json:"index"`
	Symbol string `json:"symbol"`
	Label  string `json:"label"`
	Shape  string `json:"shape"`
	Node   string `json:"node"`
}

type Op struct {
	Index  string `json:"index"`
	Symbol string `json:"symbol"`
	Source string `json:"source"`
	Target string `json:"target"`
	Node   string `json:"node"`
}

type OutVar struct {
	Index  string `json:"index"`
	Symbol string `json:"symbol"`
	Source string `json:"source"`
	Node   string `json:"node"`
}
