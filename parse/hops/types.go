package hops

type Var struct {
	Index  string `json:"index"`
	Symbol string `json:"symbol"`
	Shape  string `json:"shape"`
	Size   string `json:"size"`
	Node   string `json:"node"`
}

type Op struct {
	Index  string `json:"index"`
	Type   string `json:"type"`
	Symbol string `json:"symbol"`
	Source string `json:"source"`
	Shape  string `json:"shape"`
	Size   string `json:"size"`
	Node   string `json:"node"`
}

type OutVar struct {
	Index  string `json:"index"`
	Symbol string `json:"symbol"`
	Source string `json:"source"`
	Shape  string `json:"shape"`
	Size   string `json:"size"`
	Node   string `json:"node"`
}
