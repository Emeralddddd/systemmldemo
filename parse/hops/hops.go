package hops

import (
	"regexp"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}
func Split(data string) []string {
	r, err := regexp.Compile("\\n----[^-][\\s\\S]*?\\n")
	check(err)
	result := r.Split(data, -1)
	return result
}

func GetTread(block string) []*Var {
	r, err := regexp.Compile("\\((\\d+)\\) TRead (\\S) (\\[.*?\\]) (\\[.*?\\]), (\\S+)")
	check(err)
	var vars []*Var
	for _, x := range r.FindAllStringSubmatch(block, -1) {
		newvar := Var{
			Index:  x[1],
			Symbol: x[2],
			Shape:  x[3],
			Size:   x[4],
			Node:   x[5],
		}
		vars = append(vars, &newvar)
	}
	return vars
}

func GetOp(block string) []*Op {
	r, err := regexp.Compile("\\((\\d+)\\) (\\S+)\\((\\S+)\\) \\(([\\d+,?]+)\\) (\\[.*?\\]) (\\[.*?\\]), (\\S+)")
	check(err)
	var Ops []*Op
	for _, x := range r.FindAllStringSubmatch(block, -1) {
		newop := Op{
			Index:  x[1],
			Type:   x[2],
			Symbol: x[3],
			Source: x[4],
			Shape:  x[5],
			Size:   x[6],
			Node:   x[7],
		}
		Ops = append(Ops, &newop)
	}
	return Ops
}

func GetTwrite(block string) []*OutVar {
	r, err := regexp.Compile("\\((\\d+)\\) TWrite (\\S+) \\((\\S+)\\) (\\[.*?\\]) (\\[.*?\\]), (\\S+)")
	check(err)
	var Outvars []*OutVar
	for _, x := range r.FindAllStringSubmatch(block, -1) {
		newoutop := OutVar{
			Index:  x[1],
			Symbol: x[2],
			Source: x[3],
			Shape:  x[4],
			Size:   x[5],
			Node:   x[6],
		}
		Outvars = append(Outvars, &newoutop)
	}
	return Outvars
}
