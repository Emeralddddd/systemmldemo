package parse

import "math/rand"

func GenTestData() []*Scalar {
	var result []*Scalar
	for i := 0; i < 10; i++ {
		newScalar := Scalar{
			Iter:  i,
			Value: rand.Float64(),
		}
		result = append(result, &newScalar)
	}
	return result
}

type Scalar struct {
	Iter  int     `json:"iter"`
	Value float64 `json:"value"`
}
