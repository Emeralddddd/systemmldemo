ua AggUnaryOp.java // Aggregate unary (cell) operation: Sum (aij), col_sum, row_sum

ba AggBinaryOp.java /* Aggregate binary (cell operations): Sum (aij + bij)
                     * 		Properties:
                     * 			Inner Symbol: *, -, +, ...
                     * 			Outer Symbol: +, min, max, ...
                     * 			2 Operands
                     *
                     * 		Semantic: generate indices, align, cross-operate, generate indices, align, aggregate
                     */

                     "ba(" + outerOp.toString() + innerOp.toString()+")";

b BinaryOp.java /* Binary (cell operations): aij + bij
                              * 		Properties:
                              * 			Symbol: *, -, +, ...
                              * 			2 Operands
                              * 		Semantic: align indices (sort), then perform operation
                              */

dg DataGenOp.java /**
                   * A DataGenOp can be rand (or matrix constructor), sequence, and sample -
                   * these operators have different parameters and use a map of parameter type to hop position.
                   */

s*** DataOp.java /**
                  *  A DataOp can be either a persistent read/write or transient read/write - writes will always have at least one input,
                  *  but all types can have parameters (e.g., for csv literals of delimiter, header, etc).
                  */

q QuaternaryOp.java /**
                  * Note: this hop should be called AggQuaternaryOp in consistency with AggUnaryOp and AggBinaryOp;
                  * however, since there does not exist a real QuaternaryOp yet - we can leave it as is for now.
                  */

r ReorgOp.java /**
                *  Reorg (cell) operation: aij
                * 		Properties:
                * 			Symbol: ', rdiag, rshape, rsort
                * 			1 Operand (except sort and reshape take additional arguments)
                *
                * 		Semantic: change indices (in mapper or reducer)
                *
                *
                *  NOTE MB: reshape integrated here because (1) ParameterizedBuiltinOp requires name-value pairs for params
                *  and (2) most importantly semantic of reshape is exactly a reorg op.
                */
t TernaryOp.java /** Primary use cases for now, are
                  * 		{@code quantile (<n-1-matrix>, <n-1-matrix>, <literal>):      quantile (A, w, 0.5)}
                  * 		{@code quantile (<n-1-matrix>, <n-1-matrix>, <scalar>):       quantile (A, w, s)}
                  * 		{@code interquantile (<n-1-matrix>, <n-1-matrix>, <scalar>):  interquantile (A, w, s)}
                  *
                  * Keep in mind, that we also have binaries for it w/o weights.
                  * 	quantile (A, 0.5)
                  * 	quantile (A, s)
                  * 	interquantile (A, s)
                  *
                  * Note: this hop should be called AggTernaryOp in consistency with AggUnaryOp and AggBinaryOp;
                  * however, since there does not exist a real TernaryOp yet - we can leave it as is for now.
                  *
                  * CTABLE op takes 2 extra inputs with target dimensions for padding and pruning.
                  */
u UnaryOp.java /* Unary (cell operations): e.g, b_ij = round(a_ij)
                * 		Semantic: given a value, perform the operation (independent of other values)
                */

? DnnOp.java

FunctionOp.java
IndexingOp.java
LeftIndexingOp.java
NaryOp.java
ParameterizedBuiltinOp.java






